# Loci Logos
Repo for the source file(s) of the Loci Notes logo. The logo is made and edited in [Inkscape](https://inkscape.org/).

![logo preview](loci.svg)

## Background
* Font is standard `Serif`.